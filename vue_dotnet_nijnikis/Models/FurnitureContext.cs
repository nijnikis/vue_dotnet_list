


using Microsoft.EntityFrameworkCore;

namespace vue_dotnet_nijnikis.Models
// namespace FurnitureApi.Models
{
    public class FurnitureContext : DbContext
    {
        public FurnitureContext(DbContextOptions<FurnitureContext> options)
            : base(options)
        {
        }

        public DbSet<FurnitureItem> FurnitureItems { get; set; }
    }
}

