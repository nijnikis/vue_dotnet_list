
// yz-kawrawbky
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;

using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;

using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Design;
using Microsoft.VisualStudio.Web.CodeGeneration;
using Microsoft.VisualStudio.Web.CodeGeneration.Design;

// kastam
using VueCliMiddleware;
// using FurnitureApi.Models;
using vue_dotnet_nijnikis.Models;



namespace vue_dotnet_nijnikis
// namespace FurnitureApi
{
    public class Startup
    {

        // readonly string MyAllowSpecificOrigins = "_myAllowSpecificOrigins";

        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {

            services.AddCors(options =>
            {
                options.AddPolicy(name: "MyPolicy",
                // options.AddPolicy(name: MyAllowSpecificOrigins,
                    builder =>
                    {
                        builder.WithOrigins("http://localhost:8081",
                                            "http://localhost:8081/",
                                            "http://localhost:5000",
                                            "http://localhost:5000/",
                                            "https://localhost:8081",
                                            "https://localhost:8081/",
                                            "https://localhost:5000",
                                            "https://localhost:5000/")
                                            .AllowAnyHeader()
                                            .AllowAnyMethod();
                    });
            });

            // services.AddCors(options =>
            // {
            //     options.AddDefaultPolicy(
            //         builder =>
            //         {
            //             builder.WithOrigins("http://localhost:8081",
            //                                 "http://localhost:8081/",
            //                                 "http://localhost:5000",
            //                                 "http://localhost:5000/",
            //                                 "https://localhost:8081",
            //                                 "https://localhost:8081/",
            //                                 "https://localhost:5000",
            //                                 "https://localhost:5000/")
            //                                 .AllowAnyHeader()
            //                                 .AllowAnyMethod();
            //         });
            // });


            services.AddDbContext<FurnitureContext>(opt => opt.UseInMemoryDatabase("FurnitureList"));

            services.AddControllersWithViews();

            // In production, the React (actually, Vue, 'coz amma choosin' dat biaaach) files will be served from this directory
            services.AddSpaStaticFiles(configuration =>
            {
                configuration.RootPath = "ClientApp/dist";
            });
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IWebHostEnvironment env)
        {
            app.UseRouting();

            app.UseCors("MyPolicy");

            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }
            else
            {
                app.UseExceptionHandler("/Error");
                // The default HSTS value is 30 days. You may want to change this for production scenarios, see https://aka.ms/aspnetcore-hsts.
                app.UseHsts();
            }

            app.UseDefaultFiles();
            app.UseStaticFiles();

            // app.UseHttpsRedirection();
            app.UseStaticFiles();
            app.UseSpaStaticFiles();


            app.UseEndpoints(endpoints =>
            {
                endpoints.MapControllerRoute(
                    name: "default",
                    pattern: "{controller}/{action=Index}/{id?}");
                    // pattern: "{controller}/{action=Index}/{id?}").RequireCors(MyAllowSpecificOrigins);
                // endpoints.MapRazorPages();
            });

            app.UseSpa(spa =>
            {
                spa.Options.SourcePath = "ClientApp";

                if (env.IsDevelopment())
                {
                    // run npm process with client app
                    spa.UseVueCli(npmScript: "serve", port: 8081);
                    // if you just prefer to proxy requests from client app, use proxy to SPA dev server instead,
                    // app should be already running before starting a .NET client:
                    // spa.UseProxyToSpaDevelopmentServer("http://localhost:8080"); // your Vue app port
                }
            });
        }
    }
}
