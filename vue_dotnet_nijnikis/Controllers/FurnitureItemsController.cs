using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using vue_dotnet_nijnikis.Models;

// public static Microsoft.AspNetCore.Builder.IApplicationBuilder UseStaticFiles (this Microsoft.AspNetCore.Builder.IApplicationBuilder app);
// public static Microsoft.AspNetCore.Builder.IApplicationBuilder UseDefaultFiles (this Microsoft.AspNetCore.Builder.IApplicationBuilder app);






namespace vue_dotnet_nijnikis.Controllers
{
    // [EnableCors("MyPolicy")]
    // [Route("api/FurnitureItems")]
    [Route("api/furnitureitems")]
    // [Route("api/Controller")]
    [ApiController]
    public class FurnitureItemsController : ControllerBase
    {
        private readonly FurnitureContext _context;

        public FurnitureItemsController(FurnitureContext context)
        {
            _context = context;
        }

        // GET: api/FurnitureItems
        [HttpGet]
        public async Task<ActionResult<IEnumerable<FurnitureItem>>> GetFurnitureItems()
        {
            return await _context.FurnitureItems.OrderBy(item => item.Id).ToListAsync();
        }

        // GET: api/FurnitureItems/5
        [HttpGet("{id}")]
        public async Task<ActionResult<FurnitureItem>> GetFurnitureItem(long id)
        {
            var furnitureItem = await _context.FurnitureItems.FindAsync(id);

            if (furnitureItem == null)
            {
                return NotFound();
            }

            return furnitureItem;
        }

        // PUT: api/FurnitureItems/5
        // To protect from overposting attacks, enable the specific properties you want to bind to, for
        // more details, see https://go.microsoft.com/fwlink/?linkid=2123754.
        [HttpPut("{id}")]
        public async Task<IActionResult> PutFurnitureItem(long id, FurnitureItem furnitureItem)
        {
            if (id != furnitureItem.Id)
            {
                return BadRequest();
            }

            _context.Entry(furnitureItem).State = EntityState.Modified;

            try
            {
                await _context.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!FurnitureItemExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return NoContent();
        }

        // POST: api/FurnitureItems
        // To protect from overposting attacks, enable the specific properties you want to bind to, for
        // more details, see https://go.microsoft.com/fwlink/?linkid=2123754.


        // [HttpPost("fred-responses-report-result/{formId:int?}")]
        // public async Task<IActionResult> FredResponsesReportResult(int? formId, ReportFilters filters);


        // [HttpPost("fred-responses-report-result/{formId:int?}")]
        // public async Task<IActionResult> FredResponsesReportResult(int? formId,[Bind("ToDate","FromDate")]ReportFilters filters);

        [HttpPost]
        public async Task<ActionResult<FurnitureItem>> PostFurnitureItem(FurnitureItem furnitureItem)
        {
            _context.FurnitureItems.Add(furnitureItem);
            await _context.SaveChangesAsync();

            // return CreatedAtAction("GetFurnitureItem", new { id = furnitureItem.Id }, furnitureItem);
            return CreatedAtAction(nameof(GetFurnitureItem), new { id = furnitureItem.Id }, furnitureItem);
        }

        // DELETE: api/FurnitureItems/5
        [HttpDelete("{id}")]
        public async Task<ActionResult<FurnitureItem>> DeleteFurnitureItem(long id)
        {
            var furnitureItem = await _context.FurnitureItems.FindAsync(id);
            if (furnitureItem == null)
            {
                return NotFound();
            }

            _context.FurnitureItems.Remove(furnitureItem);
            await _context.SaveChangesAsync();

            return furnitureItem;
        }

        private bool FurnitureItemExists(long id)
        {
            return _context.FurnitureItems.Any(e => e.Id == id);
        }
    }
}
